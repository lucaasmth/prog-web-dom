<?php

require_once ("tp2-helpers.php");

function get_data($file_name) {
    $file = file($file_name);
    $csv = array_map('str_getcsv', $file);
    foreach($csv as $lineNumber => $line) {
        $csv[$lineNumber] = array_combine($csv[0], $line);
    }
    array_shift($csv);
    return $csv;
}

function proximite($lon, $lat, &$csv, $dist_max = 0){
    $repere = geopoint($lon, $lat);
    $count = 0;
    $dist_min ;
    $closer_point;
    foreach ($csv as $index => $point){
        $coord = geopoint($point["LONGITUDE"],$point["LATITUDE"]);
        $distance = distance($repere,$coord);
        $csv[$index]["DISTANCE"] = $distance;
        if ($dist_max == 0 || $distance <= $dist_max){
            if (!isset($dist_min) || $distance < $dist_min){
                $dist_min = $distance;
                $closer_point = $point;
            }
            $count ++;
            print_r("Distance de " . $point["NOM_BORNE"] . ": " . $distance . " ". $count. "ième point d'accès" . "\n");
        }
    }
    if (isset($dist_min)){
        print_r("Le point le plus proche est : ". $closer_point["NOM_BORNE"] . "\n");
    }
}

function tri_distance($data, $nombre_affichage = 0) {
    array_multisort(array_column($data, "DISTANCE"), SORT_ASC, $data);
    for($i = 0; $i < $nombre_affichage; $i++) {
        print_r($data[$i]);
    }

}

function print_adresse($data) {
    foreach($data as $point) {
        $res = smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=" . $point["LONGITUDE"] . "&lat=" . $point["LATITUDE"], 0);
        print_r(json_decode($res, true)["features"][0]["properties"]["label"] . "\n");
    }
}

if(!isset($argv[1])) {
    print_r("USAGE: " . $argv[0] . " nom_fichier\n");
    return;
}
$tab = get_data($argv[1]);
proximite(5.72752,45.19102, $tab, 200);
if(isset($argv[2])) {
    tri_distance($tab, $argv[2]);
}
print_adresse($tab);

?>