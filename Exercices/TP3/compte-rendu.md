% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : 

## Participants 

* Maida Léa
* Mathieu Lucas


1. Le format de réponse est le JSON. Il s'agit du film Fight Club. Quand on ajoute le paramètre language = fr, on s'aperçoit que la description qui était en anglais passe en français.

2. A l'aide d'un script php on affiche le résultat de la requete dans un terminal, on a utilisé la fonction tmdbget avec comme paramètre `movie/550`

3. On a premièrement ajouté un paramètre GET `id` sur la page afin de récupérer les informations du film demandé, il est défini par défaut à Fight Club (550). On a également ajouté un message d'erreur si l'id demandé n'est pas trouvé par l'API. On récupère donc le résultat de l'API et on le transforme en tableau associatif à l'aide de la fonction `json_decode()`. On utilise ensuite ce tableau associatif pour afficher les informations demandées.

4. Afin de créer un tableau tout en le rendant le plus modulable possible, nous avons décidé de créer deux tableaux dans le code, un contenant les langues que nous souhaitons afficher, et un contenant les données que nous souhaitons afficher. Dans le code nous utiliserons ensuite deux foreach afin de parcourir ces tableaux. Pour le lien, nous avons décidé d'ajouter au tableau des données reçues de l'API le lien pour facilement l'afficher par la suite.

5. Pour afficher les posters, nous utilisons la même méthode que pour le lien, nous le construisons à l'avance et nous l'ajoutons au tableau afin de les afficher facilement.

6. Pour cette question, nous avons décidé de créer une nouvelle page que nous appelons "recherche.php". On met par défaut en paramètre le film "Le Seigneur Des Anneaux". Comme précédemment, on récupère les données de l'API dans un tableau que l'on traite afin d'afficher les paramètres demandés. 

7. Pour cette question, nous avons décidé de créer une nouvelle page que nous appelons "film.php" qui utilise le même principe que le tableau de index.php mais en enlevant les langues. Nous avons une nouvelle requête qui récupère la liste des acteur du film, puis une requête par acteur qui nous permet de determiner le nombre de films dans lesquels il a joué.

8. On peut afficher uniquement les acteurs ayant "hobbit" dans le nom du personnage qu'ils jouent. Par exemple "Kissing Hobbit" joué par Zo Hartley pourra être affiché mais pas "Frodo" joué par Elijah Wood.

9. Sur la page film.php comportant la liste des acteurs, nous avons ajouté un lien sur chaque acteur redirigeant vers une page person.php affichant la liste des films auxquels il a participé ainsi que le nom de son personnage dans le film en question. Cette page était plutôt simple à réaliser étant donné qu'on utilisait déjà la requête donnant la liste des films d'un acteur afin de compter son nombre de participations à des films.

10. Dans film.php, on ajoute une requête à l'API /movie/id/videos afin de récupérer la liste des vidéos associées à un film. Puis nous affichons la 1ère à l'aide d'un iframe étant donné que les vidéos sont hébergées sur Youtube, on utilise la clé "key" afin de reconstruire l'URL de la vidéo Youtube.