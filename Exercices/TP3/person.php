<?php 

require_once("../../Helpers/tp3-helpers.php");

if (isset($_GET["id"])){
	$id = $_GET['id'];
}
else {
	$id = "550";
}

$rep_person = tmdbget("person/{$id}/movie_credits");
$person = json_decode($rep_person, true)["cast"];

if(isset($person["success"])) {
	echo "Erreur";
} else {
?>

<html>
	<head>
		<title></title>
	</head>
	<body>
		<table>
			<?php
			foreach ($person as $entry) {
				echo "<tr>";
				echo "<td><a href='film.php?id=" . $entry["id"] . "'>" . $entry["title"] . "</a> (" . $entry["character"] . ")</td>";
				echo "</tr>";
			}
			?>
		</table>
	</body>
</html>

<?php } ?>