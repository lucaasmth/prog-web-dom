<?php 

require_once("../../Helpers/tp3-helpers.php");

if (isset($_GET["id"])){
	$id = $_GET['id'];
}
else {
	$id = "550";
}

$rep_config = tmdbget("configuration");
$config = json_decode($rep_config, true);

$donneesaafficher = array(
	"title" => "Titre",
	//"original_title" => "Titre original",
	"tagline" => "Phrase d'accroche",
	"overview" => "Description",
	"lien" => "Lien",
	"poster" => "Poster",
	"trailer" => "Trailer",
	"acteurs" => "Acteurs"
);


$data;


$rep_film = tmdbget("movie/{$id}");
$film = json_decode($rep_film, true);

$rep_cast = tmdbget("movie/{$id}/credits");
$cast = json_decode($rep_cast, true)["cast"];

$rep_videos = tmdbget("movie/{$id}/videos");
$videos = json_decode($rep_videos, true)["results"];

$film["lien"] = "<a href='https://www.themoviedb.org/movie/{$id}'>Lien IMDB</a>";
$film["poster"] = "<img src='{$config["images"]["secure_base_url"]}w185{$film["poster_path"]}'>";
$film["trailer"] = "<iframe width=\"420\" height=\"315\" src=\"https:\/\/www.youtube.com/embed/{$videos[0]["key"]}\"> </iframe>";
$film["acteurs"] = "<ul>";
foreach ($cast as $acteur){
	if (isset($acteur["character"])){
		$rep_person = tmdbget("person/{$acteur["id"]}/movie_credits");
		$person = json_decode($rep_person,true)["cast"];
		$nb_film = count($person);
		$film["acteurs"] .= "<li> <a href='person.php?id={$acteur['id']}'>{$acteur["name"]}</a> ({$acteur["character"]}) $nb_film films </li>";
	}
}
$film["acteurs"] .= "</ul>";



if(isset($film["success"])) {
	echo "Erreur";
} else {
?>

<html>
	<head>
		<title></title>
	</head>
	<body>
		<table>
			<?php
			foreach ($donneesaafficher as $donneeaafficher_id => $donneeaafficher) {
				echo "<tr>";
				echo "<th>{$donneeaafficher}</th>";
				echo "<td>";
				echo $film[$donneeaafficher_id];
				echo "</td>";
				echo "</tr>";
			}
			?>
		</table>
	</body>
</html>

<?php } ?>