<?php 

require_once("../../Helpers/tp3-helpers.php");

if (isset($_GET["id"])){
	$id = $_GET['id'];
}
else {
	$id = "550";
}

$rep_config = tmdbget("configuration");
$config = json_decode($rep_config, true);

$donneesaafficher = array(
	"title" => "Titre",
	//"original_title" => "Titre original",
	"tagline" => "Phrase d'accroche",
	"overview" => "Description",
	"lien" => "Lien",
	"poster" => "Poster"
);

$langues = array(
	"" => "Original",
	"en" => "English",
	"fr" => "Français",
	"fi" => "Finnois"
);

$data;

foreach ($langues as $langue_id => $langue) {
	$rep = tmdbget("movie/{$id}", array("language" => $langue_id));
	$rep_json = json_decode($rep, true);
	$rep_json["lien"] = "<a href='https://www.themoviedb.org/movie/{$id}?language={$langue_id}'>Lien IMDB</a>";
	$rep_json["poster"] = "<img src='{$config["images"]["secure_base_url"]}w185{$rep_json["poster_path"]}'>";
	$data[$langue_id] = $rep_json;
}

//print_r($data);

if(isset($rep_json["success"])) {
	echo "Erreur";
} else {
?>

<!-- <html>
	<head>
		<title></title>
	</head>
	<body>
		<p>Title: <?php echo $rep_json["title"]; ?></p>
		<p>Original title: <?php echo $rep_json["original_title"]; ?></p>
		<?php
		if(isset($rep_json["tagline"])){
			echo "<p>Tagline: {$rep_json["tagline"]}</p>";
		}
		?>
		<p>Description: <?php echo $rep_json["overview"]; ?></p>
		<a href="https://www.themoviedb.org/movie/<?php echo $id ?>">page imdb</a> 
	</body>
</html> -->

<html>
	<head>
		<title></title>
	</head>
	<body>
		<table>
			<?php
			echo "<tr><th></th>";
			foreach($langues as $langue_id => $langue){
				echo "<th>";
				echo $langue;
				echo "</th>";
			}
			echo "</tr>";

			foreach ($donneesaafficher as $donneeaafficher_id => $donneeaafficher) {
				echo "<tr>";
				echo "<th>{$donneeaafficher}</th>";
				foreach($langues as $langue_id => $langue){
					echo "<td>";
					echo $data[$langue_id][$donneeaafficher_id];
					echo "</td>";
				}
				echo "</tr>";
			}
			?>
		</table>
	</body>
</html>

<?php } ?>